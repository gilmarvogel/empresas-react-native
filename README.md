# Desafio React Native - ioasys

### Bibliotecas utilizadas
* typescript:
 Utilizado para tipagem do javascript
* react-native-async-storage/async-storage:
Utilizado para parsistir o login do usuário.
* react-navigation/native && react-navigation/stack && react-native-screens:
 Utilizado para navegação entre telas.
* react-native-gesture-handler:
 Utilizado com as libs de navegação para gerenciamento de touchs.
* types/react-redux:
 Utilizado gerenciar o estado global da aplicação. 
* redux-saga:
 Middleware utilizado para a execução de atualizações assíncronas que interagem com a store.
* axios:
 Utilizado para a execução de requisições HTTP.
* expo:
 Utilizado para agilizar o desenvolvimento.
* eslint-config-airbnb && eslint && eslint-plugin-jsx-a11y &&  eslint-plugin-import && eslint-plugin-react && eslint-plugin-react-hooks:
 Utilizado para análise estática do código fonte.

### Como executar
* npm install
* npm start
* Instale pela playstore o aplicativo Expo, abra e escaneie o QR Code que será exibido no seu terminal ou navegador.

Opcional, baixe o apk deste link e instale no seu mobile.
Deve estar com a configuração de fontes desconhecidas habilitado.
https://drive.google.com/file/d/1wP7rTqSrhuBfMzRAc1xAPa5hWg67Njme/view?usp=sharing

Login no app
* User: testeapple@ioasys.com.br
* Password: 12341234

### Bônus
* Utilização de Redux / Redux Saga (realizado)
* Utilização de linters ou outras ferramentas de análise estática(realizado)
* Testes unitários, interface, (parcialmente)